﻿namespace RockPaperScissors.Model
{
    public enum GameState
    {
        Connecting,
        Connected,
        Playing,
        Ended
    }
}