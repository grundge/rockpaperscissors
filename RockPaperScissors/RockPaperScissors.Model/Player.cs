﻿using System;

namespace RockPaperScissors.Model
{
    public class Player
    {
        public string Name { get; set; }
        public Guid PlayerGuid { get; set; }
        public Play GamePlay { get; set; }
        public string Message { get; set; }
        public GameState PlayerGameState { get; set; }
        public Guid GameSession { get; set; }
        public WinState WinState { get; set; }
    }
}