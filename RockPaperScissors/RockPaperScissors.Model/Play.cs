﻿namespace RockPaperScissors.Model
{
    public enum Play
    {
        Rock,
        Paper,
        Scissors
    }
}