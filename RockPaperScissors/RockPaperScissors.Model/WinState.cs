﻿namespace RockPaperScissors.Model
{
    public enum WinState
    {
        Tied,
        Won,
        Lost
    }
}