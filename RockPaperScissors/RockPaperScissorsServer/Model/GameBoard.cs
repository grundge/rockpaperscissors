﻿using System;
using System.Collections.Generic;
using RockPaperScissors.Model;

namespace RockPaperScissorsServer.Model
{
    public class GameBoard
    {
        public List<GameSession> GameSessions;

        public GameBoard()
        {
            GameSessions = new List<GameSession>();
        }

        public GameSession CreateNewGameSession()
        {
            GameSession gameSession = new GameSession();
            gameSession.GameSessionId = Guid.NewGuid();
            gameSession.Plays = 0;
            GameSessions.Add(gameSession);
            return gameSession;
        }

        public Player WhoWon(Player p1, Player p2)
        {
            switch (p1.GamePlay)
            {
                case Play.Paper when p2.GamePlay == Play.Scissors:
                    return p2;
                case Play.Paper when p2.GamePlay == Play.Rock:
                    return p1;
                case Play.Rock when p2.GamePlay == Play.Paper:
                    return p2;
                case Play.Rock when p2.GamePlay == Play.Scissors:
                    return p1;
                case Play.Scissors when p2.GamePlay == Play.Rock:
                    return p2;
                case Play.Scissors when p2.GamePlay == Play.Paper:
                    return p1;
                default:
                    // If none of the conditions above returned a player, it means they both played the same hand and tied for this turn
                    return null;
            }
        }
    }
}