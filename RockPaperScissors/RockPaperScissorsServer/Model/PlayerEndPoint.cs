﻿using System.Net;
using RockPaperScissors.Model;

namespace RockPaperScissorsServer.Model
{
    public class PlayerEndPoint
    {
        public IPEndPoint IpEndPoint { get; set; }
        public Player Player { get; set; }
    }
}