﻿using System;
using System.Collections.Generic;
using RockPaperScissors.Model;
using RockPaperScissorsServer.Settings;

namespace RockPaperScissorsServer.Model
{
    public class GameSession
    {
        public Guid GameSessionId { get; set; }
        public int Plays { get; set; }
    }
}