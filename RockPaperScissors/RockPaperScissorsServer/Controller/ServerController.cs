﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RockPaperScissors.Model;
using RockPaperScissorsServer.Model;
using RockPaperScissorsServer.Settings;

namespace RockPaperScissorsServer.Controller
{
    public class ServerController
    {
        private List<PlayerEndPoint> _playerEndPoints;
        private GameBoard _gameBoard;
        
        public ServerController()
        {
            _playerEndPoints = new List<PlayerEndPoint>();
            _gameBoard = new GameBoard();
        }

        public void Start()
        {
            TaskRunner();
            Console.WriteLine("Server started...");
        }

        private async void TaskRunner()
        {
            await Task.Run(ProcessMessages);
        }

        private async Task ProcessMessages()
        {
            while (true)
            {
                var result = await ReceiveUdpMessage();
                if (result.Buffer != null)
                {
                    Player player = GetPlayerMessage(result.Buffer);
                    switch (player.PlayerGameState)
                    {
                        case GameState.Connecting:
                            Player newPlayer = _playerEndPoints
                                .FirstOrDefault(u => u.IpEndPoint.Address.Equals(result.RemoteEndPoint.Address))
                                ?.Player;
                            // The player joined the server and we can find him in the list
                            // If the player is not on the list, it means that we did not add him because
                            // the server was full. This should be refactored when working with TCP sockets.
                            if (newPlayer != null)
                            {
                                Console.WriteLine(player.Message);
                                player.Message = "Welcome to Rock Paper Scissors Game! Please wait until the game stars.";
                                player.PlayerGameState = GameState.Connected;
                                byte[] messageToSend = SetPlayerMessage(player);
                                await SendUdpMessage(messageToSend, result.RemoteEndPoint);
                            }
                            break;
                        case GameState.Connected:
                            if (_playerEndPoints.Count == ServerSettings.MaximumLobbyPlayers)
                            {
                                GameSession newGameSession = _gameBoard.CreateNewGameSession();
                                foreach (PlayerEndPoint playerEndPoint in _playerEndPoints)
                                {
                                    playerEndPoint.Player.GameSession = newGameSession.GameSessionId;
                                    playerEndPoint.Player.Message = "The game will start";
                                    playerEndPoint.Player.PlayerGameState = GameState.Playing;
                                    byte[] messageToSend = SetPlayerMessage(playerEndPoint.Player);
                                    await SendUdpMessage(messageToSend, playerEndPoint.IpEndPoint);
                                }
                            }
                            break;
                        case GameState.Playing:
                            GameSession currentGameSession =
                                _gameBoard.GameSessions.FirstOrDefault(gs => gs.GameSessionId == player.GameSession);
                            currentGameSession.Plays++;
                            // The maximum players is recommended to be two, given then playing rules
                            if (currentGameSession.Plays == ServerSettings.MaximumLobbyPlayers)
                            {
                                // The second player has the same game session as the first player and
                                // has a different endPoint
                                // This is not the best approach into finding out which is which
                                // but we will keep this for now
                                PlayerEndPoint p2 = _playerEndPoints.FirstOrDefault(p =>
                                    !p.IpEndPoint.Address.Equals(result.RemoteEndPoint.Address) 
                                    && p.Player.GameSession == player.GameSession);
                                
                                Player playerWhoWon = _gameBoard.WhoWon(player, p2.Player);
                                if (playerWhoWon == null)
                                {
                                    p2.Player.WinState = WinState.Tied;
                                    p2.Player.Message = "You tied. Please play again.";
                                    byte[] tied1MessageToSend = SetPlayerMessage(p2.Player);
                                    await SendUdpMessage(tied1MessageToSend, p2.IpEndPoint);
                                        
                                    player.WinState = WinState.Tied;
                                    player.Message = "You tied. Please play again.";
                                    byte[] tied2MessageToSend = SetPlayerMessage(player);
                                    await SendUdpMessage(tied2MessageToSend, result.RemoteEndPoint);
                                }
                                else
                                {
                                    if (playerWhoWon.PlayerGuid == player.PlayerGuid)
                                    {
                                        p2.Player.WinState = WinState.Lost;
                                        p2.Player.Message = "You lost the game.";
                                        p2.Player.PlayerGameState = GameState.Ended;
                                        byte[] lostMessageToSend = SetPlayerMessage(p2.Player);
                                        await SendUdpMessage(lostMessageToSend, p2.IpEndPoint);
                                        
                                        player.WinState = WinState.Won;
                                        player.Message = "You won the game.";
                                        player.PlayerGameState = GameState.Ended;
                                        byte[] wonMessageToSend = SetPlayerMessage(player);
                                        await SendUdpMessage(wonMessageToSend, result.RemoteEndPoint);
                                    }
                                    else
                                    {
                                        p2.Player.WinState = WinState.Won;
                                        p2.Player.Message = "You won the game.";
                                        p2.Player.PlayerGameState = GameState.Ended;
                                        byte[] lostMessageToSend = SetPlayerMessage(p2.Player);
                                        await SendUdpMessage(lostMessageToSend, p2.IpEndPoint);
                                        
                                        player.WinState = WinState.Lost;
                                        player.Message = "You lost the game.";
                                        player.PlayerGameState = GameState.Ended;
                                        byte[] wonMessageToSend = SetPlayerMessage(player);
                                        await SendUdpMessage(wonMessageToSend, result.RemoteEndPoint);
                                    }
                                }
                            }
                            break;
                        case GameState.Ended:
                            // todo: remove players from the players list
                            // cleanup connections
                            break;
                    }
                }
                Thread.Sleep(100);
            }
        }

        private byte[] SetPlayerMessage(Player player)
        {
            string jsonString = JsonConvert.SerializeObject(player);
            return Encoding.UTF8.GetBytes(jsonString);
        }
        
        private Player GetPlayerMessage(byte[] byteMessage)
        {
            string jsonString = Encoding.UTF8.GetString(byteMessage);
            return JsonConvert.DeserializeObject<Player>(jsonString);
        }

        private async Task SendUdpMessage(byte[] messageBytes, IPEndPoint ipEndPoint)
        {
            using (UdpClient client = new UdpClient())
            {
                await client.SendAsync(messageBytes, messageBytes.Length, new IPEndPoint(ipEndPoint.Address, ServerSettings.SendMessagePort));
            }
        }

        private async Task<UdpReceiveResult> ReceiveUdpMessage()
        {
            UdpReceiveResult udpReceiveResult;
            using (var client = new UdpClient(ServerSettings.ReceiveMessagePort))
            {
                udpReceiveResult =  await client.ReceiveAsync();
            }

            if (_playerEndPoints.
                    FirstOrDefault(
                        u => u.IpEndPoint.Address.Equals(udpReceiveResult.RemoteEndPoint.Address)) 
                == null)
            {
                if (_playerEndPoints.Count >= ServerSettings.MaximumPlayers)
                {
                    // todo: return a server is full message
                    // If a player is sent a server is full with Connecting state we can handle it on the client
                }
                else
                {
                    PlayerEndPoint playerEndPoint = new PlayerEndPoint()
                    {
                        IpEndPoint = udpReceiveResult.RemoteEndPoint,
                        Player = new Player()
                        {
                            PlayerGuid = Guid.NewGuid()
                        }
                    };
                    _playerEndPoints.Add(playerEndPoint);
                }
            }
            
            return udpReceiveResult;
        }
    }
}