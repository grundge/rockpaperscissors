﻿using System.Threading;
using System.Threading.Tasks;
using RockPaperScissorsServer.Controller;

namespace RockPaperScissorsServer
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            ServerController serverController = new ServerController();
            serverController.Start();
            while (true)
            {
                Thread.Sleep(100);
            };
        }
    }
}