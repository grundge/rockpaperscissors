﻿namespace RockPaperScissorsServer.Settings
{
    public static class ServerSettings
    {
        public const int SendMessagePort = 5000;
        public const int ReceiveMessagePort = 6000;
        public const int MaximumPlayers = 100;
        public const int MaximumLobbyPlayers = 1;
    }
}