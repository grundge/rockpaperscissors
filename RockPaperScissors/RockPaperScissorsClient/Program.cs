﻿using System.Threading;
using System.Threading.Tasks;
using RockPaperScissorsClient.Controller;

namespace RockPaperScissorsClient
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            ClientController clientController = new ClientController();
            clientController.Start();
            while (clientController.IsPlaying)
            {
                Thread.Sleep(100);
            };
        }
    }
}