﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RockPaperScissors.Model;
using RockPaperScissorsClient.Settings;

namespace RockPaperScissorsClient.Controller
{
    public class ClientController
    {
        private readonly IPEndPoint _serverIpEndPoint;
        private Player _player;
        public bool IsPlaying = true;
        
        public ClientController()
        {
            _serverIpEndPoint = new IPEndPoint(IPAddress.Parse(ClientSettings.IpAddress), ClientSettings.SendMessagePort);
            _player = new Player();
        }

        public void Start()
        {
            TaskRunner();
        }
        
        private async void TaskRunner()
        {
            await Task.Run(ProcessMessages);
        }
        
        private async Task ProcessMessages()
        {
            _player.Message = "Hello Server";
            byte[] helloServerMessageByte = SetPlayerMessage(_player);
            await SendUdpMessage(helloServerMessageByte);
            
            while (IsPlaying)
            {
                var result = await ReceiveUdpMessage();
                if (result.Buffer != null)
                {
                    Player player = GetPlayerMessage(result.Buffer);
                    switch (player.PlayerGameState)
                    {
                        case GameState.Connecting:
                            // If the server returns a server is full with connecting state, we can handle that here.
                            break;
                        case GameState.Connected:
                            _player = player;
                            Console.WriteLine(player.Message); // The client will not wait for the server to initiate the game
                            _player.Message = "Ready to play";
                            byte[] playerConnectedMessage = SetPlayerMessage(_player);
                            await SendUdpMessage(playerConnectedMessage);
                            break;
                        case GameState.Playing:
                            Console.WriteLine(player.Message); // The server has initiated the game, the client may now play
                            Console.WriteLine("Please play 0 - Rock, 1 - Paper, 2 - Scissors");
                            // The following instructions can and should be enhanced. We should make sure that the player chose a valid option
                            string playString = Console.ReadLine();
                            Play play = (Play)int.Parse(playString);
                            _player.GamePlay = play;
                            byte[] playerMessage = SetPlayerMessage(_player);
                            await SendUdpMessage(playerMessage);
                            break;
                        case GameState.Ended:
                            Console.WriteLine(player.Message);
                            IsPlaying = false;
                            break;
                    }
                }
                Thread.Sleep(100);
            }
        }
        
        private byte[] SetPlayerMessage(Player player)
        {
            string jsonString = JsonConvert.SerializeObject(player);
            return Encoding.UTF8.GetBytes(jsonString);
        }
        
        private Player GetPlayerMessage(byte[] byteMessage)
        {
            string jsonString = Encoding.UTF8.GetString(byteMessage);
            return JsonConvert.DeserializeObject<Player>(jsonString);
        }

        private async Task SendUdpMessage(byte[] messageBytes)
        {
            using (var client = new UdpClient())
            {
                await client.SendAsync(messageBytes, messageBytes.Length, _serverIpEndPoint);
            }
        }

        private async Task<UdpReceiveResult> ReceiveUdpMessage()
        {
            UdpReceiveResult udpReceiveResult;
            using (var client = new UdpClient(ClientSettings.ReceiveMessagePort))
            {
                udpReceiveResult =  await client.ReceiveAsync();
            }

            return udpReceiveResult;
        }
    }
}