﻿namespace RockPaperScissorsClient.Settings
{
    public static class ClientSettings
    {
        public const int SendMessagePort = 6000;
        public const int ReceiveMessagePort = 5000;
        public const string IpAddress = "192.168.1.9";
    }
}