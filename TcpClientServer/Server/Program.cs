﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Server
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, 9000);
            TcpListener tcpListener = new TcpListener(ipEndPoint);
            tcpListener.Start();

            TcpClient tcpClient = tcpListener.AcceptTcpClient();

            byte[] byteMessage = new byte[1024];
            
            // Option 1 to read message
            int numberOfBytesRead = 0;
            StringBuilder myCompleteMessage = new StringBuilder();
            NetworkStream stream = tcpClient.GetStream();
            do{
                numberOfBytesRead = stream.Read(byteMessage, 0, byteMessage.Length);

                myCompleteMessage.AppendFormat("{0}", Encoding.ASCII.GetString(byteMessage, 0, numberOfBytesRead));
            }
            while(stream.DataAvailable);
            // Option 2 to receive message
            //tcpClient.Client.Receive(byteMessage);
            
            //string message = Encoding.ASCII.GetString(byteMessage);
            // Console.WriteLine(message);
            
            Console.WriteLine(myCompleteMessage);

            byte[] messageToSend = Encoding.ASCII.GetBytes("Hello from server");
            
            // Option 1 to read message
            stream.Write(messageToSend, 0, messageToSend.Length);
            // Option 2 to read message
            //tcpClient.Client.Send(messageToSend);

            // Send files
            tcpClient.Client.SendFile("image.jpg");
            
            tcpClient.Close();
            tcpListener.Stop();
        }
    }
}