﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Client
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            TcpClient tcpClient = new TcpClient();
            tcpClient.Connect(IPAddress.Parse("172.16.8.240"), 9000);

            tcpClient.Client.Send(Encoding.ASCII.GetBytes("Hello from client"));
            
            byte[] byteMessage = new byte[1024];
            tcpClient.Client.Receive(byteMessage);
            string message = Encoding.ASCII.GetString(byteMessage);
            
            Console.WriteLine(message);
            
            NetworkStream networkStream = tcpClient.GetStream();
            
            using (var output = File.Create("result.zip"))
            {
                Console.WriteLine("Client connected. Starting to receive the file");

                // read the file in chunks of 1KB
                var buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = networkStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    output.Write(buffer, 0, bytesRead);
                }
            }
            
            tcpClient.Close();
        }
    }
}